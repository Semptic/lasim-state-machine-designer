package de.lasim.statemachine.designer.gui

import javafx.application.Application
import javafx.event.{ActionEvent, EventHandler}
import javafx.scene.control._
import javafx.scene.layout.{HBox, VBox, BorderPane}
import javafx.scene.{Node, Scene, Group}
import javafx.stage.Stage
import javafx.scene.input.{MouseButton, MouseEvent}
import javafx.scene.paint.Color
import javafx.beans.value.{ObservableValue, ChangeListener}
import javafx.beans.property.SimpleStringProperty

class MainWindow extends Application {
	override def start(primaryStage: Stage) = {
		primaryStage.setTitle("State Machine Designer")

		primaryStage.setScene(new Scene(new BorderPane {
			val root = this

			val statePane = new Group with FocusPane {
				setStyle("-fx-border-color:black; -fx-border-width:2; -fx-border-insets:0;")
				getChildren.add(new Label)

				override def toString() = s"StatePane"
			}

			setCenter(new ScrollPane {
				setContent(statePane)
			})

			setTop(new Button("Create state") {
				addEventHandler(ActionEvent.ACTION, new EventHandler[ActionEvent] {
					def handle(ae: ActionEvent): Unit = {
						statePane.getChildren.add(new State with Focusable {
							override val returnNode: Node = this
							override val focusPane: FocusPane = statePane

							override def onLooseFocus(): Unit = {setColor(Color.RED)}
							override def onGetFocus(): Unit = setColor(Color.GREEN)

							override def toString() = s"StackPane from ${this.name.get()}"
						})
					}
				})
			})

			setRight(new VBox {
				val vBox = this

				val width = 200f

				setStyle("-fx-border-color:blue; -fx-border-width:1; -fx-border-insets:0;")

				getChildren.addAll(new Label {
					statePane.addChangeListener(new ChangeListener[Focusable] {
						def changed(property: ObservableValue[_ <: Focusable], oldValue: Focusable, newValue: Focusable): Unit = {
							if (newValue.isInstanceOf[State]) setText("Zustandsautomat")
							else if (newValue.isInstanceOf[Arrow]) setText("Übergang")
							requestFocus()
						}
					})
				}, new HBox {
					val textField = new TextField {
						var stateNameBinding: SimpleStringProperty = null

						statePane.addChangeListener(new ChangeListener[Focusable] {
							def changed(property: ObservableValue[_ <: Focusable], oldValue: Focusable, newValue: Focusable): Unit = {
								if (stateNameBinding != null) {
									textProperty().unbindBidirectional(stateNameBinding)
									stateNameBinding = null
								}

								textProperty().unbind()
								clear()

								if (newValue.isInstanceOf[State]) {
									stateNameBinding = newValue.asInstanceOf[State].name
									textProperty().bindBidirectional(stateNameBinding)
								} else if (newValue.isInstanceOf[Arrow]) {
									textProperty().bind(newValue.asInstanceOf[Arrow].fromState.name.concat(" to ").concat(newValue.asInstanceOf[Arrow].toState.name))
								}
							}
						})
					}

					getChildren.addAll(new Label {
						setText("Name:")
						setLabelFor(textField)
						addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
							def handle(me: MouseEvent): Unit = {
								if (me.getButton == MouseButton.PRIMARY) {
									textField.requestFocus()
									me.consume()
								}
							}
						})
					}, textField)

				}, new ScrollPane {
					val scrollPane = this

					setContent(new VBox {
						val vBox = this

						statePane.addChangeListener(new ChangeListener[Focusable] {
							def changed(property: ObservableValue[_ <: Focusable], oldValue: Focusable, newValue: Focusable): Unit = {
								vBox.getChildren.removeAll()
								vBox.getChildren.clear()

								if (newValue.isInstanceOf[State]) {
									for (transition <- newValue.asInstanceOf[State].transitionsTo) vBox.getChildren.add(new Label {
											setText(transition.toString)
											addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler[MouseEvent] {
												def handle(me: MouseEvent): Unit = {
													if (me.getButton == MouseButton.PRIMARY) {
														transition.asInstanceOf[Focusable].setFocus()
														me.consume()
													}
												}
											})
									})
								}
							}
						})
					})
				})
			})

		}, 600, 600))

		primaryStage.show()
	}
}

object MainWindow {
	def main(args: Array[String]) = {
		Application.launch(classOf[MainWindow], args: _*)
	}
}
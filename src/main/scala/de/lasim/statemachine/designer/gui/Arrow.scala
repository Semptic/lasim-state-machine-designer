package de.lasim.statemachine.designer.gui

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.value.{ObservableValue, ChangeListener}
import javafx.scene.Group
import javafx.scene.shape._
import javafx.scene.shape.Path
import javafx.scene.paint.Color

class Arrow extends Group  {
	val arrow = this

	val tailX = new SimpleDoubleProperty()
	val tailY = new SimpleDoubleProperty()

	val headX = new SimpleDoubleProperty()
	val headY = new SimpleDoubleProperty()

	val leftOffset = new SimpleDoubleProperty()
	val rightOffset = new SimpleDoubleProperty()

	var fromState: State = _
	var toState: State = _

	private val realStartX = new SimpleDoubleProperty()
	private val realStartY = new SimpleDoubleProperty()

	private val realEndX = new SimpleDoubleProperty()
	private val realEndY = new SimpleDoubleProperty()

	private val angle = new SimpleDoubleProperty()

	private val path = new Path
	private val moveTo = new MoveTo
	private val arcTo = new ArcTo

	def setColor(color: Color) = {path.setStroke(color); polygon.setStroke(color); polygon.setFill(color)}

	path.getElements().addAll(moveTo, arcTo)

	private val polygon = new Polygon {
		getPoints().addAll(0.0, -10.0, -5.0, 10.0, 5.0, 10.0)
		layoutXProperty().bind(arcTo.xProperty())
		layoutYProperty().bind(arcTo.yProperty())
	}

	getChildren().addAll(polygon, path)

	tailX.addListener(new ChangeListener[Number] {
		def changed(p1: ObservableValue[_ <: Number], p2: Number, p3: Number): Unit = {
			realStartX.set(p3.asInstanceOf[Double])
			angle.set(Math.atan2(realEndY.get() - realStartY.get(), realEndX.get() - realStartX.get()))
		}
	})

	tailY.addListener(new ChangeListener[Number] {
		def changed(p1: ObservableValue[_ <: Number], p2: Number, p3: Number): Unit = {
			realStartY.set(p3.asInstanceOf[Double])
			angle.set(Math.atan2(realEndY.get() - realStartY.get(), realEndX.get() - realStartX.get()))
		}
	})

	headX.addListener(new ChangeListener[Number] {
		def changed(p1: ObservableValue[_ <: Number], p2: Number, p3: Number): Unit = {
			realEndX.set(p3.asInstanceOf[Double])
			angle.set(Math.atan2(realEndY.get() - realStartY.get(), realEndX.get() - realStartX.get()))
		}
	})

	headY.addListener(new ChangeListener[Number] {
		def changed(p1: ObservableValue[_ <: Number], p2: Number, p3: Number): Unit = {
			realEndY.set(p3.asInstanceOf[Double])
			angle.set(Math.atan2(realEndY.get() - realStartY.get(), realEndX.get() - realStartX.get()))
		}
	})

	angle.addListener(new ChangeListener[Number] {
		def changed(p1: ObservableValue[_ <: Number], p2: Number, p3: Number): Unit = {
			val x: Double = realEndX.get() - realStartX.get()
			val y: Double = realEndY.get() - realStartY.get()
			val r: Double = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2))

			moveTo.setX((r + leftOffset.get()) * Math.cos(angle.get()) + 2 * realStartX.get() - realEndX.get())
			moveTo.setY((r + leftOffset.get()) * Math.sin(angle.get()) + 2 * realStartY.get() - realEndY.get())

			arcTo.setX(((r - 7.5) - rightOffset.get()) * Math.cos(angle.get()) + realStartX.get())
			arcTo.setY(((r - 7.5) - rightOffset.get()) * Math.sin(angle.get()) + realStartY.get())

			arcTo.setRadiusX(r)
			arcTo.setRadiusY(r)

			polygon.setRotate(angle.get().toDegrees + 90 - Math.sqrt(r))
		}
	})
}
package de.lasim.statemachine.designer.gui

import javafx.scene.Node
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.value.{ObservableValue, ChangeListener}
import javafx.scene.input.MouseEvent
import javafx.event.EventHandler

trait FocusPane {
	val self = this

	private val focus = new SimpleObjectProperty[Focusable] {
		addListener(new ChangeListener[Focusable] {
			def changed(property: ObservableValue[_ <: Focusable], oldValue: Focusable, newValue: Focusable): Unit = {
				if (oldValue != null) {
					if (newValue == null || oldValue != newValue.getParent) {
						oldValue.onLooseFocus()

						if (oldValue.isInstanceOf[FocusPane]) {
							def recursive(target: FocusPane): Unit = {
								if (target != null) {
									val child = target.getFocus()
									if (child != null) {
										child.onLooseFocus()
										if (child.isInstanceOf[FocusPane]) {
											recursive(child.asInstanceOf[FocusPane])
										}
									}
								}
							}
							recursive(oldValue.asInstanceOf[FocusPane])
						}

						if (oldValue.getParent.isInstanceOf[FocusPane]) {
							def recursive(target: FocusPane): Unit = {
								if (target != null) {
									if (target.isInstanceOf[Focusable]) {
										target.asInstanceOf[Focusable].onLooseFocus()

										val parent = target.asInstanceOf[Focusable].getParent

										if (parent != null) {
											if (parent.isInstanceOf[FocusPane]) {
												recursive(parent.asInstanceOf[FocusPane])
											}
										}
									}
								}
							}
							recursive(oldValue.getParent.asInstanceOf[FocusPane])
						}
					}
				}

				if (newValue != null) {
					newValue.onGetFocus()

					if (newValue.isInstanceOf[FocusPane]) {
						def recursive(target: FocusPane): Unit = {
							if (target != null) {
								val child = target.getFocus()
								if (child != null) {
									child.onGetFocus()
									if (child.isInstanceOf[FocusPane]) {
										recursive(child.asInstanceOf[FocusPane])
									}
								}
							}
						}
						recursive(newValue.asInstanceOf[FocusPane])
					}

					if (newValue.getParent.isInstanceOf[FocusPane]) {
						def recursive(target: FocusPane): Unit = {
							if (target != null) {
								if (target.isInstanceOf[Focusable]) {
									target.asInstanceOf[Focusable].onGetFocus()

									val parent = target.asInstanceOf[Focusable].getParent

									if (parent != null) {
										if (parent.isInstanceOf[FocusPane]) {
											recursive(parent.asInstanceOf[FocusPane])
										}
									}
								}
							}
						}
						recursive(newValue.getParent.asInstanceOf[FocusPane])
					}
				}
			}
		})
	}

	def resetFocus() = {println("reset focus"); focus.set(null)}

	def addChangeListener(listener: ChangeListener[Focusable]) = focus.addListener(listener)

	def removeChangeListener(listener: ChangeListener[Focusable]): Unit = removeChangeListener(listener)

	def setFocus(node: AnyRef): Unit = focus.set(node.asInstanceOf[Focusable])

	def getFocus(): Focusable = {
		focus.get()
	}
}

trait Focusable extends Node{
	val sgfsag = this

	def onGetFocus(): Unit = ()
	def onLooseFocus(): Unit = ()

	val focusPane: FocusPane
	val returnNode: Node

	def setFocus() = focusPane.setFocus(this)

	addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
		def handle(me: MouseEvent): Unit = {
			setFocus()
		}
	})
}
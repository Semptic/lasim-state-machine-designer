package de.lasim.statemachine.designer.gui

import javafx.event.EventHandler
import javafx.scene.input.{MouseButton, MouseEvent}
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.scene.shape.Circle
import scala.collection.mutable.ListBuffer
import javafx.beans.property.SimpleStringProperty
import javafx.scene.control.Label
import javafx.scene.{Node, Group}

private object id {
	private var id = 0
	def get(): Integer = {
		val out = id
		id += 1
		out
	}
}
class State extends Group with FocusPane {
	private val state = this

	var openTransition: Arrow = null

	var dragOffsetX = 0d
	var dragOffsetY = 0d

	var mouseAnchorX = 0d
	var mouseAnchorY = 0d

	val transitionsTo = new ListBuffer[State]
	val transitionsFrom = new ListBuffer[State]

	var dragging = false

	var name = new SimpleStringProperty()

	name.set(s"State${id.get()}")

	private var circleColor = Color.RED

	def setColor(color: Color) = { circleColor = color; circle.setFill(circleColor)}

	private val circle = new Circle {
		setRadius(40)
		setFill(circleColor)
		setOpacity(1)
	}

	private val label = new Label {
		textProperty().bind(name)
	}

	override def contains(x: Double, y: Double): Boolean = {
		val centerX = state.getLayoutX + state.getTranslateX + circle.getRadius
		val centerY = state.getLayoutY + state.getTranslateY + circle.getRadius

		Math.sqrt(Math.pow(centerX - x, 2) + Math.pow(centerY - y, 2)) < circle.getRadius
	}

	addEventHandler(MouseEvent.ANY, new EventHandler[MouseEvent] {
		def handle(me: MouseEvent): Unit = {
			if (me.getButton == MouseButton.PRIMARY) {
				me.getEventType match {
					case MouseEvent.MOUSE_PRESSED => {
						mouseAnchorX = me.getSceneX
						mouseAnchorY = me.getSceneY
						dragOffsetX = getTranslateX
						dragOffsetY = getTranslateY
					}
					case MouseEvent.MOUSE_DRAGGED => {
						dragging = true
						circle.opacityProperty().set(0.2)
						var notDropHere = false

						for (child <- state.getChildrenUnmodifiable.toArray if !notDropHere && child.isInstanceOf[State] && child != state) {
							val diffX = Math.abs((dragOffsetX - mouseAnchorX + me.getSceneX + getLayoutX) - (child.asInstanceOf[State].getTranslateX + child.asInstanceOf[State].getLayoutX))
							val diffY = Math.abs((dragOffsetY - mouseAnchorY + me.getSceneY + getLayoutX) - (child.asInstanceOf[State].getTranslateY + child.asInstanceOf[State].getLayoutY))
							val minDistance = 2 * circle.getRadius + 10

							if (Math.sqrt(Math.pow(diffX,2)+Math.pow(diffY,2)) < minDistance) {
								notDropHere = true
							}
						}

						setTranslateX(dragOffsetX - mouseAnchorX + me.getSceneX)
						setTranslateY(dragOffsetY - mouseAnchorY + me.getSceneY)

						if (notDropHere) {
							circle.setFill(Color.BLACK)
						} else {
							circle.setFill(circleColor)
						}
					}
					case MouseEvent.MOUSE_RELEASED => {
						var notDropHere = false

						dragging = false

						for (child <- state.getParent.getChildrenUnmodifiable.toArray if !notDropHere && child.isInstanceOf[State] && child != state) {
							val diffX = Math.abs((dragOffsetX - mouseAnchorX + me.getSceneX + getLayoutX) - (child.asInstanceOf[State].getTranslateX + child.asInstanceOf[State].getLayoutX))
							val diffY = Math.abs((dragOffsetY - mouseAnchorY + me.getSceneY + getLayoutX) - (child.asInstanceOf[State].getTranslateY + child.asInstanceOf[State].getLayoutY))
							val minDistance = 2 * circle.getRadius + 10

							if (Math.sqrt(Math.pow(diffX,2)+Math.pow(diffY,2)) < minDistance) {
								notDropHere = true
							}
						}

						if (notDropHere) {
							setTranslateX(dragOffsetX)
							setTranslateY(dragOffsetY)
						}

						dragOffsetX = 0d
						dragOffsetY = 0d
						circle.setFill(circleColor)
						circle.opacityProperty().set(1)
					}
					case _ =>
				}
			}
		}
	})

	addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler[MouseEvent] {
		def handle(me: MouseEvent): Unit = {
			if (me.getButton == MouseButton.SECONDARY) {
				openTransition = new Arrow with Focusable {
					leftOffset.set(circle.getRadius)
					tailX.bind(circle.radiusProperty())
					tailY.bind(circle.radiusProperty())
					headX.set(me.getX)
					headY.set(me.getY)

					setMouseTransparent(true)

					override val returnNode: Node = this
					override val focusPane: FocusPane = state

					override def onLooseFocus(): Unit = setColor(Color.BLACK)
					override def onGetFocus(): Unit = { setColor(Color.GREEN)
						if (state.getParent.isInstanceOf[FocusPane]) {
							state.getParent.asInstanceOf[FocusPane].setFocus(this)
						}
					}

					override def toString() = s"Arrow from ${state.name.get()}"
				}
				state.getChildren.add(openTransition)
			}
		}
	})

	addEventHandler(MouseEvent.MOUSE_DRAGGED, new EventHandler[MouseEvent] {
		def handle(me: MouseEvent): Unit = {
			if (me.getButton == MouseButton.SECONDARY) {
				openTransition.headX.set(me.getX)
				openTransition.headY.set(me.getY)
			}
		}
	})

	addEventHandler(MouseEvent.MOUSE_RELEASED, new EventHandler[MouseEvent] {
		def handle(me: MouseEvent): Unit = {
			if (me.getButton == MouseButton.SECONDARY) {
				if (openTransition != null) {
					var found = false

					for (child <- state.getParent.getChildrenUnmodifiable.toArray if !found && child.isInstanceOf[State]) {
						val target = child.asInstanceOf[State]

						val absX = state.getLayoutX + state.getTranslateX + me.getX
						val absY = state.getLayoutY + state.getTranslateY + me.getY

						if (state != target && target.contains(absX, absY)) {
							if (!transitionsTo.contains(target)) {
								openTransition.rightOffset.set(circle.getRadius)

								val targetX = target.layoutXProperty.add(target.translateXProperty)
								val targetY = target.layoutYProperty.add(target.translateYProperty)

								val selfX = state.layoutXProperty.add(state.translateXProperty)
								val selfY = state.layoutYProperty.add(state.translateYProperty)

								val arrowX = targetX.add(selfX.multiply(-1)).add(target.circle.radiusProperty)
								val arrowY = targetY.add(selfY.multiply(-1)).add(target.circle.radiusProperty)

								openTransition.headX.bind(arrowX)
								openTransition.headY.bind(arrowY)

								state.transitionsTo += target
								target.transitionsFrom += state

								openTransition.toState = target
								openTransition.fromState = state

								openTransition.setMouseTransparent(false)

								found = true
							}
						}
					}
					if (!found) {
						state.getChildren.remove(openTransition)
					}
					openTransition = null
				}
			}
		}
	})

	override def toString = name.get()

	getChildren.addAll(new StackPane with Focusable {
		getChildren.addAll(circle, label)
		setPickOnBounds(false)

		override val returnNode: Node = state
		override val focusPane: FocusPane = state

		override def toString() = s"StackPane from ${state.name.get()}"
	})
}